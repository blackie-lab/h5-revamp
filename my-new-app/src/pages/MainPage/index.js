import React, { useState } from 'react';
import Sidebar from '../../components/Sidebar';
import './index.css';

class MainPage extends React.Component {
  // const [isOpen, setIsOpen] = useState(false);

  constructor(props) {
    super(props);
    this.state = { isOpenSidebar: false};
    this.toggleSidebar = this.toggleSidebar.bind(this);
  }

  toggleSidebar(){
    this.setState({isOpenSidebar:!this.state.isOpenSidebar})
  }
  
  render(){
    return (
        <div className="container-fluid mt-3">
            <nav className="navbar navbar-expand-lg navbar-light bg-white shadow-md">
                <div className="container-fluid p-2">
                    <a className="navbar-brand text-primary mr-0">Company Logo</a>
                    <div className="form-inline ml-auto">
                        <div className="btn btn-primary" onClick={()=>this.toggleSidebar()} >
                            <i className="fa fa-bars"></i>
                        </div>
                    </div>
                </div>
            </nav>
            <Sidebar isActive={this.state.isOpenSidebar} handleSidbarToggle={()=>this.toggleSidebar()} />
      </div>
  )}
}

export default MainPage;
