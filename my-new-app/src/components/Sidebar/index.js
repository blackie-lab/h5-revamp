import React from 'react';

class ToggleSidebar extends React.Component {

    constructor(props) {
        super(props);
    }

    render(){
        const toggleSidebar = this.props.handleSidbarToggle;
        
        return (
            <div>
                <div className={`sidebar ${this.props.isActive == true ? 'active' : ''}`}>
                    <div className="header">
                        <h4 className="mb-0">火熱</h4>
                        <div className="btn btn-primary" onClick={()=>toggleSidebar()}><i className="fa fa-times"></i></div>
                    </div>
                    <div className="body">
                        <nav className='hotGameTypeMenu'>
                            <div className="tab-pane"><img src="https://cdn-icons-png.flaticon.com/64/1828/1828884.png"></img><text>熱門遊戲排行</text></div>
                            <div className="tab-pane"><img src="https://cdn-icons-png.flaticon.com/64/1828/1828884.png"></img><text>新遊推薦</text></div>
                            <div className="tab-pane"><img src="https://cdn-icons-png.flaticon.com/64/1828/1828884.png"></img><text>我收藏的遊戲</text></div>
                        </nav>
                        <div className='hotGameList'>
                            <ul>
                                <li>
                                    <a className="sd-link">
                                        <figure>
                                            <img src="https://cdn-icons-png.flaticon.com/64/1828/1828884.png"></img>
                                            <div>
                                                <figcaption>Game 1</figcaption>
                                                <p>1234567890</p>
                                            </div>
                                            <button>打開</button>
                                        </figure>
                                    </a>
                                </li>
                                <li>
                                    <a className="sd-link">
                                        <figure>
                                            <img src="https://cdn-icons-png.flaticon.com/64/1828/1828884.png"></img>
                                            <div>
                                                <figcaption>Game 1</figcaption>
                                                <p>1234567890</p>
                                            </div>
                                            <button>打開</button>
                                        </figure>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className={`sidebar-overlay ${this.props.isActive == true ? 'active' : ''}`} onClick={()=>toggleSidebar()}></div>
            </div>
    )}
}

export default ToggleSidebar;